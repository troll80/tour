import time
import random
from collections import namedtuple

from selenium.webdriver.support.ui import  WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from browser import BookingSite, Cookies, Hotel, Result
from browser import logger


def fillEqualHuman(elem, text):
    for word in text:
        secounds = random.gauss(0.8, 0.2)
        time.sleep(secounds**2)
        elem.send_keys(word)


class Coral(BookingSite):

    def authorizated(self):
        elem = self._driver.find_element_by_xpath(".//table[@id='ctl00_ContentPlaceHolder1_UserName']//input")
        fillEqualHuman(elem, self.config['LOGIN'])
        elem = self._driver.find_element_by_xpath(".//table[@id='ctl00_ContentPlaceHolder1_UserPassword']//input")
        fillEqualHuman(elem, self.config['PASSWORD'])
        self._driver.find_element_by_xpath(".//table[@id='ctl00_ContentPlaceHolder1_GirisBtn']").click()
        logger.debug('Authorizate done.')

    def parsePage(self, country):
        table = WebDriverWait(self._driver, 120).until(EC.presence_of_element_located((By.ID, "cbpOnlyHotelSearch_results")))
        #logger.debug('Table. {}'.format(table.size))
        items = table.find_elements_by_tag_name('tr')
        logger.debug('Cnt items:{}'.format(len(items)))
        time.sleep(3)
        result = list()

        for item in items[1:]:
            tds = item.find_elements_by_tag_name('td')
            if(len(tds) == 9):
                room = ''.join(tds[4].text[:-5])
                hotel = tds[2].text.split('\n')[0].strip()
                if Hotel.isCheckRoom(country, hotel, room):
                    logger.debug('Check: HOTEL: {} ROOM:{}'.format(hotel, room))
                    result.append(dict(
                    country = country,
                    room = room,
                    hotel = hotel,
                    meal = 'BB' if tds[3].text in 'Завтрак' else tds[3].text ,
                    price = tds[7].text
                ))
        return result

    def _run_process(self):
        self._driver.get(self.config['URL'])
        self.authorizated()
        self._driver.find_element_by_xpath(".//td[@id='tdTabHotel']/a").click()
        logger.debug('Check tab hotel')
        time.sleep(5)
        ## wait span id='Label4' === 'Отели'
        elem = WebDriverWait(self._driver, 30).until(EC.presence_of_element_located((By.XPATH, ".//select[@id='toCountry_SEL']")))
        for country in Hotel.getCountries():
            Select(elem).select_by_visible_text(country)
            logger.debug('Country:{}'.format(country))
            #expand all item
            #group_items = self._driver.find_elements_by_xpath(".//div[@id='lb_Are']/div/span/img")
            #for item in group_items:
            #    item.click()

            #items = self._driver.find_elements_by_xpath(".//div[@id='lb_Are']//label")
            #for item in items:
            #     if item.text in self.config['PLASES'].split(',') and item.text != '':
            #        logger.debug('Plases choseed:{}'.format(item.text))
            #        item.click()

            ## set date from/to hotel
            self._driver.execute_script("document.getElementById('txtDateFromHotel').value='{}'".format(self.config['DATEFROMHOTEL']))
            logger.debug('Set date from hotel:{}'.format(self.config['DATEFROMHOTEL']))
            self._driver.execute_script("document.getElementById('txtDateToHotel').value='{}'".format(self.config['DATETOHOTEL']))
            logger.debug('Set date to hotel:{}'.format(self.config['DATEFROMHOTEL']))

            ## select adults
            Select(self._driver.find_element_by_id('cbAdult_SEL')).select_by_visible_text(str(self.config['ADULT']))
            logger.debug('Selected adult:{}'.format(self.config['ADULT']))
            ## select hights
            Select(self._driver.find_element_by_id('cbNightBegin_SEL')).select_by_visible_text(str(self.config['NIGHTBEGIN']))
            logger.debug('Selected night begin:{}'.format(self.config['NIGHTBEGIN']))
            time.sleep(1)
            Select(self._driver.find_element_by_id('cbNightEnd_SEL')).select_by_visible_text(self.config['NIGHTEND'])
            logger.debug('Selected night end:{}'.format(self.config['NIGHTEND']))
            ## currency
            Select(self._driver.find_element_by_id('cbCurrency_SEL')).select_by_visible_text(self.config['currency'])
            logger.debug('Selected currency: USD')

            ## type meal
            for meal in self.config['MEAL'].split(','):
                if meal != '':
                    self._driver.find_element_by_xpath(".//label[text()='{}']".format(meal)).click()
                    logger.debug('Choose meal:{}'.format(meal))

            ## category
            category_table = self._driver.find_element_by_id('lb_Category_TBODY')
            for category in self.config['CATEGORY'].split(','):
                if category != '':
                   category_table.find_element_by_xpath(".//label[text()='{}']".format(category)).click()
                   logger.debug('Choosed category:{}'.format(category))


            #type_table = self._driver.find_element_by_id('lb_RoomType')
            #for type_ in self.config['TYPES'].split(','):
            #    if type_ != '':
            #        type_table.find_element_by_xpath(".//label[text()='{}']".format(type_)).click()
            #        logger.debug('Choosed types:{}'.format(type_))


            ## choose hotels
            items = self._driver.find_elements_by_xpath(".//tbody[@id='lb_Hot_TBODY']/tr/td/label")
            for item in items:
                #item_text = item.text.strip().lower()
                #logger.debug("Hotel {}".format(item.text))
                if Hotel.include(item.text, country):
                    item.click()
                    logger.debug('Choosing the hotel: {}'.format(item.text))

            self._driver.find_element_by_id('_BtnProductSearch_TD_1').click()
            #time.sleep(1)
            results = list()
            while True:
                time.sleep(3)
                results.extend(self.parsePage(country))
                try:
                    self._driver.find_element_by_link_text('Следующая страница »').click()
                    logger.debug('Next page')
                except NoSuchElementException:
                    logger.debug('Stopped iter pages')
                    break

            Result.push('CORAL', results)

if __name__ == '__main__':
    Coral.run('CORAL', headless=False)
