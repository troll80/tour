import time
import random

from selenium.webdriver.support.ui import  WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from browser import Driver, BookingSite, Cookies, Hotel, Result
from browser import logger


def fillEqualHuman(elem, text):
    for word in text:
        secounds = random.gauss(0.8, 0.2)
        time.sleep(secounds**2)
        elem.send_keys(word)


class DnataTour(BookingSite):

    def authorizated(self):
        elem = self._driver.find_element_by_xpath(".//input[@name='username']")
        fillEqualHuman(elem, self.config['login'])

        elem = self._driver.find_element_by_xpath(".//input[@name='password']")
        fillEqualHuman(elem, self.config['password'])
        time.sleep(2)
        self._driver.find_element_by_xpath(".//button[@type='submit']").click()

    def _run_process(self):
        self._driver.get(self.config['URL'])
        if False:#Cookies.is_exist('dnata'):
            self.setCookies('dnata')
            self._driver.refresh()
        else:
            self.authorizated()
            Cookies.add(self._driver.get_cookies())
        ## WAIT
        time.sleep(3)
        for country in Hotel.getCountries():
            ## HOTEL
            for hotel in Hotel.get(country):
                ##
                ##DATE

                #elems = self._driver.find_elements_by_xpath(".//div[@class='date-picker']/div/div/input")
                #elems = self._driver.find_elements_by_xpath(".//div[@class='field-with-icon']//input")
                elems = self._driver.find_elements_by_xpath(".//div[@class='date-picker']/div/div/input[@name='date']")
                #elems[0].send_keys(self.config['DATEBEGIN'])

                #while elems[0].text != self.config['DATEBEGIN']:
                #    elems[0].clear()
                #    elems[0].send_keys(self.config['DATEBEGIN'])
                #a = input()
                time.sleep(3)
                elems[1].clear()
                elems[1].send_keys(self.config['DATEEND'])
                elems[0].send_keys(self.config['DATEBEGIN'])
                #while elems[1].text != self.config['DATEEND']:
                #    elems[1].clear()
                #    elems[1].send_keys(self.config['DATEEND'])

                logger.info('Date begin: {} Date end: {}'.format(self.config['DATEBEGIN'], self.config['DATEEND']))
                ##HOTEL
                elem = self._driver.find_element_by_xpath(".//div[@class='search-unit-field destination']//div//div//input")
                elem.clear()
                time.sleep(1)
                elem.send_keys(hotel[0])
                elem.click()
                time.sleep(1)
                try:
                    self._driver.find_element_by_xpath(".//li[@class='field-options-item']").click()
                except:
                    pass
                logger.info('Hotel: {}'.format(hotel[0]))
                ## SEARCH
                self._driver.find_element_by_xpath(".//div[@class='search-unit-field submit']/button").click()
                #time.sleep(10)
                try:
                    elem = WebDriverWait(self._driver, 60).until(EC.visibility_of_element_located((By.ID, "roomsGrid")))
                except TimeoutException:
                    logger.error('Error wait:{}'.format(hotel[0]))
                    continue

                elems = elem.find_elements_by_class_name('hotel-room-availability')
                results = list()
                for elem in elems:
                    room = elem.find_element_by_class_name('room-header').text
                    if Hotel.isCheckRoom(country, hotel[0], room):
                        tbody = elem.find_elements_by_tag_name('tbody')
                        for row in tbody:
                            tds = row.find_elements_by_tag_name('td')
                            price = tds[2].text.split('H')[0]
                            meal = tds[0].text
                            if price:
                                results.append(dict(
                                country = country,
                                hotel = hotel[0],
                                meal = meal,
                                room = room,
                                price = price))

                Result.push('DNATA', results)
                self._driver.execute_script("window.history.go(-1)")


if __name__ == '__main__':
    DnataTour.run('DNATA', headless = True)
