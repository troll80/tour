import time
import random
from collections import namedtuple

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotInteractableException

from browser import BookingSite, Cookies, Hotel, Result
from browser import logger

## behavior
def doEqHuman():
    seconds = random.gauss(1, 0.2)
    time.sleep(seconds * seconds)


class TezTour(BookingSite):

    def closePushUpMsg(self):
        try:
            iframe = self._driver.find_element_by_xpath(".//div[@class='lgwg-new-popup-shadow']/parent::div/iframe")
            self._driver.switch_to.frame(iframe)
            self._driver.find_element_by_class_name('close-btn-x-new-dot').click()
            self._driver.switch_to.default_content()
        except:
            pass


    def parsePage(self, country):
        table = WebDriverWait(self._driver, 120).until(EC.visibility_of_element_located((By.ID, "st-result-table")))
        #logger.debug('Table:{}'.format(table.size))
        #table = self._driver.find_element_by_id('st-result-table')
        items = table.find_elements_by_class_name('st-result-row')
        logger.debug('Found {} items'.format(len(items)))

        result = list()
        for row in items:
            tds = row.find_elements_by_tag_name('td')
            room = tds[2].find_element_by_tag_name('small').text
            hotel = tds[2].text.split('\n')[0]
            if Hotel.isCheckRoom(country, hotel, room):
                result.append(dict(
                   country = country,
                   room = room,
                   hotel = hotel,
                   meal =  'BB' if tds[6].text in 'Только завтраки' else tds[6].text,
                  price = tds[8].find_element_by_tag_name('a').text.split('\n')[1]
             ))
        return result

    def _run_process(self):

        self._driver.get(self.config['URL'])
        #if Cookies.is_exist():
        #   self.setCookies()
        #    self._driver.refresh()
        try:
            WebDriverWait(self._driver, 10).until(EC.element_to_be_clickable(( By.ID, 'changeCity'))).click()
        except:
            logger.error('Choose city')
        #choose city src
        # initial
        logger.debug('Waiting download main page.')
        time.sleep(3)
        try:
            elem = self._driver.find_element_by_id('cities')
        except StaleElementReferenceException as e:
            logger.error(e)
        else:
            Select(elem).select_by_visible_text(self.config['City'])
        doEqHuman()
        #self.closePushUpMsg()
        #Select(self._driver.find_element_by_id('cities')).select_by_visible_text(self.config['City'])

        for country in Hotel.getCountries():
            #self.closePushUpMsg()
            Select(self._driver.find_element_by_id('countries')).select_by_visible_text(country)
            logger.debug('Selected countries: {}'.format(country))
            #doEqHuman()
            time.sleep(20)
            self.closePushUpMsg()
            try:
                Select(self._driver.find_element_by_id('kindSpo')).select_by_visible_text(self.config['kindSpo'])
            except:
                Select(self._driver.find_element_by_id('kindSpo')).select_by_visible_text('Проживание+трансфер')
            doEqHuman()
            #self.closePushUpMsg()
            logger.debug('Selected kindSpo: {}'.format(self.config['kindSpo']))
            #Select(self._driver.find_element_by_id('st-hc')).select_by_visible_text(self.config['stHC'])
            #logger.debug('Selected stHC: {}'.format(self.config['stHC']))
            Select(self._driver.find_element_by_id('st-rb')).select_by_visible_text(self.config['stRB'])
            logger.debug('Selected stRB: {}'.format(self.config['stRB']))
            #doEqHuman()
            #self.closePushUpMsg()
            self._driver.find_element_by_id('st-rb-better-checkbox').click()
            logger.debug('Chosen not better  meal')
            #doEqHuman()
            #self.closePushUpMsg()
            Select(self._driver.find_element_by_id('accommodation')).select_by_visible_text(self.config['accommodation'])
            logger.debug('Selected accommodation: {}'.format(self.config['accommodation']))
            #doEqHuman()
            #self.closePushUpMsg()
            elem = self._driver.find_element_by_id('st-after')
            elem.clear()
            #doEqHuman()
            #self.closePushUpMsg()
            elem.send_keys(self.config['stAfter'])
            logger.debug('Set stAfter: {}'.format(self.config['stAfter']))
            #doEqHuman()
            #self.closePushUpMsg()

            elem = self._driver.find_element_by_id('st-before')
            elem.clear()
            #doEqHuman()

            elem.send_keys(self.config['stBefore'])
            logger.debug('Set stBefore: {}'.format(self.config['stBefore']))
            #doEqHuman()
            elem = self._driver.find_element_by_id('st-nights-min')
            elem.clear()
            #doEqHuman()
            elem.send_keys(self.config['stHightsMin'])
            logger.debug('Set stHightsMin: {}'.format(self.config['stHightsMin']))

            #doEqHuman()
            elem = self._driver.find_element_by_id('st-nights-max')
            elem.clear()
            elem.send_keys(self.config['stHightsMax'])
            logger.debug('Set stHightsMax: {}'.format(self.config['stHightsMax']))
            Select(self._driver.find_element_by_id('st-currency-select')).select_by_visible_text(self.config['currency'])
            logger.debug('Selected currency:{}'.format(self.config['currency']))
            # choose otels
            list_hotels = WebDriverWait(self._driver, 30).until(EC.presence_of_element_located((By.ID, "st-all-hotels-list")))
            #list_hotels = self._driver.find_element_by_id('st-all-hotels-list')
            items = list_hotels.find_elements_by_class_name('st-checkable-item')

            for item in items:
                if Hotel.include(item.text, country):
                    item.find_element_by_xpath(".//input[not(contains(@style,'display:none'))]").click()
                    #item.find_element_by_tag_name('input').text#click()
                    logger.info('Item: {}'.format(item.text))
                else:
                    logger.debug('Pass hotel :{}'.format(item.text))

            elem = self._driver.find_element_by_class_name('gStatTourSearch')
            elem.click()
            #logger.debug(elem.size)
            Cookies.add(self.cookies)
            logger.debug('Searching')
            #time.sleep(10)
            #### WAIT
            results = list()
            while True:
                results.extend(self.parsePage(country))
                try:
                    self._driver.find_element_by_id("moreresultsA").click()
                except ElementNotInteractableException:
                    logger.debug('Stopped iter page')
                    break

            Result.push('TEZ', results)

if __name__ == '__main__':
    TezTour.run('TEZ', headless=False)
