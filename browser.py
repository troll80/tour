import sys
import os
import time
import re
import random
from datetime import datetime
import logging
from importlib import import_module
import importlib.util
import pickle
import configparser
import csv
from collections import OrderedDict
from fuzzywuzzy import fuzz

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import  WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException


FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=FORMAT)
#logging.basicConfig(filename='logs/log.txt', level=logging.DEBUG, format=FORMAT)
logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)
#app = Celery('tasks', backend='amqp', broker='amqp://')
__HOTELS = dict()


def prText(text):
    s = text.lower()
    reg = re.compile('[^a-zA-Z ]')
    return reg.sub('', s)

class Hotel:
    @staticmethod
    def loadConfig():
        config = configparser.ConfigParser()
        config.optionxform = str
        config.read('config.ini')
        global __HOTELS
        __HOTELS = dict()
        for item in config:
            if item not in ['TEZ', 'DNATA', 'ARTTOUR', 'CORAL', 'DEFAULT']:
                __HOTELS[item] = list()
                for hotel, room in config[item].items():
                    __HOTELS[item].append((hotel, room))

    @staticmethod
    def get(country):
        return __HOTELS.get(country)

    @staticmethod
    def getCountries():
        logger.debug(__HOTELS.keys())
        return __HOTELS.keys()

    @staticmethod
    def include(hotel, country):
        if hotel != '':
            for hotel_, room_ in __HOTELS[country]:
                #logger.info('{}: {} | {}'.format(fuzz.token_set_ratio(prText(hotel_), prText(hotel)), prText(hotel_), prText(hotel)))
                #print(hotel_)

                ratio = fuzz.token_set_ratio(prText(hotel_), prText(hotel))
                if ratio > 90:
                    #logger.debug('Selecting ratio {}: {} in {}'.format(ratio, hotel_, room_))
                    return True
                else:
                    pass
                    #logger.debug('Pass@ ratio {}: {} | {}'.format(ratio, prText(hotel_), prText(hotel)))

        return False

    @staticmethod
    def isCheckRoom(country, hotel, room):
        for hotel_, room_ in  __HOTELS[country]:
             if prText(hotel_) in prText(hotel):
                 if prText(room_) in prText(room):
                     #print('EQ item:{} hotel:{} room:{}'.format(item, hotel, room))
                     return True
                 else:
                     return False

        return False

## LOAD GLOBAL CONFIG
Hotel.loadConfig()
logger.debug(__HOTELS)


class Result:
    """ save results in table
     """
    def __init__(self):
        self.fields = ['country', 'hotel', 'meal', 'room']
        self.table = list()
        self._read()

    def _read(self):
        if not os.path.exists('result.csv'):
            return

        with open('result.csv') as f:
            reader = csv.DictReader(f,  delimiter=';')
            for row in reader:
                self.table.append(OrderedDict(row))

    def getRow(self, d_input):

        for row in self.table:
            #if all([d_input[key] == row[key] for key in self.fields]):
            #    return row
            if prText(d_input['hotel']) == prText(row['hotel']) \
               and prText(d_input['room']) == prText(row['room']):
                self.table.remove(row)
                return row

        logger.info('In table added {}'.format(d_input.items()))
        try:
            fieldnames = self.table[0]
        except IndexError:
            fieldnames = self.fields
        return OrderedDict([(key, d_input.get(key, 'n/a')) for key in fieldnames])

    def save(self):
        with open('result.csv', 'w') as f:
            fieldnames = self.table[0].keys()
            #logger.debug('FIELDNAMES:{}'.format(fieldnames))
            writer = csv.DictWriter(f, fieldnames=fieldnames,  delimiter=';')
            writer.writeheader()
            for row in self.table:
                writer.writerow(row)

            logger.debug('File saved.')

    @classmethod
    def push(cls, section, l_input):
        r = cls()
        table = list()
        for item in l_input:
            row = r.getRow(item)
            row[section] = item['price']
            table.append(row)
            #logger.debug('Row: {}'.format(row))
        for item in r.table:
            table.append(item)

        r.table = table
        r.save()


class Browser:
    @staticmethod
    def create(name_browser):
        lib_path = 'selenium.webdriver.{}.webdriver'.format(name_browser)
        if importlib.util.find_spec(lib_path) is None:
            logger.error('No found module browser')
            raise Exception()

        browser = import_module(lib_path)
        options = browser.Options()
        logger.info('Browser: {}'.format(name_browser))
        return browser, options


class Driver:
    @staticmethod
    def create(name_browser, headless):
        browser, options = Browser.create(name_browser)
        options.headless = headless
        driver = browser.WebDriver(options=options)
        driver.maximize_window()
        #driver.wait = WebDriverWait(driver, 60)
        driver.implicitly_wait(10)
        return driver


class DriverConn:
    def __init__(self, name, headless=True):
        self.name = name
        self.headless = headless
        self.driver = None

    def __enter__(self):
        self.driver = Driver.create(self.name, self.headless)
        return self.driver

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val:
            self.driver.save_screenshot('logs/screenshot_{}.png'.format(datetime.now()))
            self.driver.close()
            raise Exception('{}:{}'.format(exc_type, exc_val))


class Cookies:
    @staticmethod
    def add(cookies):
        with open('cookies.pkl', 'ab') as f:
            pickle.dump(cookies, f)

    @staticmethod
    def is_exist():
        return os.path.isfile('cookies.pkl')

    @staticmethod
    def getAll():
        with open('cookies.pkl', 'rb') as f:
            return pickle.load(f)


class BookingSite:
    def __init__(self, section):
        self._driver = None
        self.loadSetting(section)

    def setCookies(self):
        #self._driver.get(self._url)
        #self._driver.delete_all_cookies()
        for cookie in Cookies.getAll():
            self._driver.add_cookie(cookie)

        #self._driver.refresh()
        logger.debug('Cookies append in browser.')

    def setDriver(self, driver):
        self._driver = driver

    @property
    def cookies(self):
        return self._driver.get_cookies()

    def _run_process(self):
        pass

    def loadSetting(self, section):
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.config = config[section]

    @classmethod
    def run(cls, section, headless=True):
        up = cls(section)
        with DriverConn('firefox', headless) as driver:
            up.setDriver(driver)
            up._run_process()

#class Meal:
#    @staticmethod
#    def parse(text):
#        if ''

if __name__ == '__main__':
    #BookingSite.run(headless=True)
    print(Hotel.getCountries())
