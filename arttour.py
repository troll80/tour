import time
from selenium.common.exceptions import NoSuchElementException
from browser import BookingSite, Hotel, Result
from browser import logger


class ArtTour(BookingSite):
    def select(self, xpath, text):
            self._driver.find_element_by_xpath("{}//a[@class='chosen-single']".format(xpath)).click()
            [elem.click() for elem in self._driver.find_elements_by_xpath("{}//ul[@class='chosen-results']/li".format(xpath)) if elem.text == text]

    def _run_process(self):
        self._driver.get(self.config['URL'])
        self._driver.find_element_by_xpath(".//div[@data-searchmode='hotel']").click()
        #self._driver.find_element_by_class_name("chosen-container").click()

        for country in Hotel.getCountries():
            self.select(".//td[text()='страна']/parent::tr//div[contains(@class, 'STATEINC_chosen')]", country)
            logger.debug('Chosen country: {}'.format(country))
            ## date
            self._driver.execute_script("document.getElementsByClassName('CHECKIN_BEG')[0].value='{}'".format(self.config['CHECKIN_BEG']))
            self._driver.execute_script("document.getElementsByClassName('CHECKIN_END')[0].value='{}'".format(self.config['CHECKIN_END']))
            logger.debug('Dates: checkin begin {} checkin end: {}'.format(self.config['CHECKIN_BEG'], self.config['CHECKIN_END']))

            ## nights
            self.select(".//td[text()='ночей от']/parent::tr/td[@class='nights']", self.config['NIGHTFROM'])
            logger.debug('Chosen nights: {}'.format(self.config['NIGHTFROM']))
            self.select(".//td[text()='до']/parent::tr/td[@class='nights']", self.config['NIGHTSTILL'])
            logger.debug('Chosen nights till: {}'.format(self.config['NIGHTSTILL']))
            ## adult
            self.select(".//td[text()='взрослых']/parent::tr/td[@class='tourists']", self.config['ADULT'])
            ## currency
            self.select(".//td[@class='cost']", self.config['CURRENCY'])
            logger.debug('CURRENCY: {}'.format(self.config['CURRENCY']))
            ## hotels
            elem = self._driver.find_element_by_xpath(".//div[@name='HOTELS']")
            items = elem.find_elements_by_tag_name('label')
            for item in items:
                if Hotel.include(item.text, country):
                    item.click()
            ## meal
            elems = self._driver.find_elements_by_xpath(".//div[@name='MEALS']/label")
            for elem in elems:
                if elem.text.strip() in self.config['MEALS']:
                    elem.click()
                    logger.debug('Checked MEAL:{}'.format(elem.text))

            ## search
            self._driver.find_element_by_xpath(".//button[text() = 'Искать']").click()
            logger.debug('Searching. waiting results')
            time.sleep(10)
            ## results
            table = self._driver.find_elements_by_xpath(".//table[@class='res']/tbody/tr")
            results = list()
            for tr in table:
                tds = tr.find_elements_by_tag_name('td')
                hotel =  tds[3].text.split("(")[0].strip()
                room = tds[6].text
                if Hotel.isCheckRoom(country, hotel, room):
                    results.append(dict(
                    country = country,
                    hotel = hotel,
                    meal = tds[5].text,
                    room = room.split('/')[0],
                    price = tds[9].text)
                )
            Result.push('ARTTOUR', results)


if __name__ == '__main__':
    ArtTour.run('ARTTOUR', headless=False)
